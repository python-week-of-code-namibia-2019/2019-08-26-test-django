# Python Week Of Code, Namibia 2019

Django’s unit tests use Python's unittest,
the same library that we use in the previous section.

## Task for Instructor

1. Add

   ```
   class PostTestCase(TestCase):
       def setUp(self):
           Post.objects.create(
               title="Welcome",
               text="This is your first website."
           )

       def test_post_title(self):
           post = Post.objects.get(
               title="Welcome"
           )
           self.assertEqual(
               lion.title,
               'Welcome'
           )
   ```

   to [blog/tests.py](blog/tests.py).
2. Run

   ```
   python manage.py test
   ```
3. Add

   ```
   from django.test import Client

   c = Client()
   ```

   and

   ```
       def test_post_client(self):
           response = c.get('/blog/welcome/')
           self.assertEqual(
               response.status_code,
               200
           )
   ```

   to [blog/tests.py](blog/tests.py).
4. Run

   ```
   python manage.py test
   ```

## Tasks for Learners

1. Add a test to the index page.